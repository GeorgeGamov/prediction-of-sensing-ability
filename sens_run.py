# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import functions from scripts ------------------------------

import sens_data
import sens_preproc
import sens_levenshtein
import sens_doc2vec
import sens_postproc
import sens_writer
import sens_tanimoto
import sens_euc_dist
import sens_get_cat_for_fp
import sens_draw

# function ----------------------------------------------------

def sens_run(_subdir, _db_file, _q_file, _file_out, N_max, _frame, dict):
    
    # load data
    
    _database, _query, _method = sens_data.sens_scripts_load(_subdir, _db_file, _q_file)
    
    # demonstrate distribution of cation (table and plot)
    
    df_one_cation = sens_get_cat_for_fp.sens_get_cat_for_fp(_database)
    sens_get_cat_for_fp.sens_cat_distr(df_one_cation)
    
    # preprocess
    
    _db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, _q_InChI, _q_InChIKey = sens_preproc.sens_preproc(_database, _query, _q_file)
      
    # processing with different metrics
    
    if _method == 3:
        res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max, df_best_levenshtein = sens_levenshtein.sens_levenstein_proc(_db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, _q_InChI, _q_InChIKey, N_max)
        
    elif _method == 4:
        res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max, df_best_doc2vec = sens_doc2vec.sens_token_doc2vec(_db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, _q_InChI, _q_InChIKey, N_max, _frame, dict)
        
    elif _method == 1:
        
        df_best_tanimoto = sens_tanimoto.sens_tanimoto(_q_SMILES, df_one_cation)
        
    elif _method == 2:
        
        df_best_euclidean = sens_euc_dist.sens_euc_dist(_q_SMILES, df_one_cation) 
        
    else:
        raise Exception("Unknown method")    
    
    # postproc
    if _method == 3:
        #df_out = sens_postproc.sens_postproc(res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max, _q_SMILES, _q_InChI, _q_InChIKey, N_max)
        df_out = df_best_levenshtein
      
    elif _method == 4:
        #df_out = sens_postproc.sens_postproc(res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max, _q_SMILES, _q_InChI, _q_InChIKey, N_max)
        df_out = df_best_doc2vec
        
    elif _method == 1:
        df_out = df_best_tanimoto
        
    elif _method == 2:
        df_out = df_best_euclidean
        
    
    # write data
    if _method == 3:
        sens_writer.sens_output(_subdir, _file_out, df_out)
    elif _method == 4:
        sens_writer.sens_output(_subdir, _file_out, df_out)
        
    # draw molecules
    sens_draw.sens_draw_mol(df_out)
      
    #return res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max