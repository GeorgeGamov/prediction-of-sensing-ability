# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import textwrap
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
from copy import deepcopy
import numpy as np
import pandas as pd
import re

# replacing special symbols by spaces ------------------------
def sens_multiple_replace(dict, text):
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))
    
    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)

# tokenizing the databse -------------------------------------
# and user query and finding cosine similarity ---------------

def sens_token_doc2vec(_db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, 
                _q_InChI, _q_InChIKey, N_max, _frame, dict):
    
    _db_InChIKey = np.array(_db_InChIKey).tolist()
    res_ret_cation = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_medium = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_source = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    sim_N_max = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    indices_N_max = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_db_SMILES = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    _db_SMILES_work, _q_SMILES_work = deepcopy(_db_SMILES), deepcopy(_q_SMILES)
    
    # dividing SMILES into tokens and replacing two-character chemical symbols by single character
    for i in range(len(_db_SMILES_work)):

        _db_SMILES_work[i] = sens_multiple_replace(dict, _db_SMILES_work[i])
        _db_SMILES_work[i] = ' '.join(textwrap.wrap(_db_SMILES_work[i], _frame))
        
    _db_SMILES_work = _db_SMILES_work.tolist()
             
    for i in range(len(_q_SMILES_work)):
        _q_SMILES_work[i] = sens_multiple_replace(dict, _q_SMILES_work[i])
        _q_SMILES_work[i] = ' '.join(textwrap.wrap(_q_SMILES_work[i], _frame))
        
    #_q_SMILES_work = _q_SMILES_work.tolist()
    
    # uniting the lists to unify the vectorization
    total_SMILES = _db_SMILES_work + _q_SMILES_work

    # using doc2vec for training model and finding similarity
    
    # tagging the data
    documents = [TaggedDocument(words=word_tokenize(_d), tags=[str(i)]) for i, _d in enumerate(total_SMILES)]
    
    #initializing the model
    d2v_model = Doc2Vec(vector_size=100, min_count=2, epochs=80, alpha=0.025, window=5)
    
    #building vocabulary of tagged data
    voca = d2v_model.build_vocab(documents)
    
    #training and saving doc2vec
    d2v_model.train(documents, total_examples=d2v_model.corpus_count, epochs=80)
    d2v_model.save("d2v.model")
    
    #loading model for using
    model = Doc2Vec.load("d2v.model")

    #finding the similar values
    max_sim_matrix = [[0 for i in range(N_max)] for j in range(len(_q_SMILES_work))]
    for j in range(len(_q_SMILES_work)):
        similar_doc = model.docvecs.most_similar(str(len(_db_InChIKey)+j), topn=N_max)
        
        for i in range(N_max):
            max_sim_matrix[j][i] = int(similar_doc[i][0])
            sim_N_max[j][i] = similar_doc[i][1]
            
            res_ret_cation[j][i] = _db_cation[max_sim_matrix[j][i]]
            res_ret_medium[j][i] = _db_medium[max_sim_matrix[j][i]]
            res_ret_source[j][i] = _db_source[max_sim_matrix[j][i]]  
            res_ret_db_SMILES[j][i] = _db_SMILES[max_sim_matrix[j][i]]
        
        df_best_doc2vec = pd.DataFrame(np.hstack(((np.array(res_ret_db_SMILES)).reshape(len(_q_SMILES) * N_max, 1), (np.array(res_ret_cation, dtype = object)).reshape(len(_q_SMILES) * N_max, 1), (np.array(sim_N_max)).reshape(len(_q_SMILES) * N_max, 1))), index = max_sim_matrix[0], columns = ['SMILES','Cation','cosine_similarity'])
        
        df_best_doc2vec['Cation'] = df_best_doc2vec['Cation'].apply(lambda x: ','.join(x))
        
    return(res_ret_cation, res_ret_medium, res_ret_source, sim_N_max, res_ret_db_SMILES, df_best_doc2vec)