# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------

import pandas as pd
from openpyxl import load_workbook
import io
import os
import pdb
import sys
sys.path.append(os.path.abspath('../kev/'))

# basic input ------------------------------------------------

def sens_scripts_load(_subdir, _db_file, _q_file):
    # if specific file selected it should be XLSX one
    if _db_file != "":
        if _subdir != '':
            _db_subdir = '/' + _subdir
            _db_subdir = '../../input' + _db_subdir + '/'
            _db_file = _db_subdir + _db_file
        else:
            _db_file = _db_file
    else:
        print("File cannot be found")
    
    # open database excel file
    with open(_db_file, "rb") as f:
        inmemory_file = io.BytesIO(f.read())
    wb = load_workbook(inmemory_file, read_only = True)
    
    _database = pd.read_excel(_db_file, sheet_name = "database", index_col = None)
        
    # open user query file
    if _q_file != "":
        if _subdir != '':
            _q_subdir = '/' + _subdir 
        _q_subdir = '../../input/' + _q_subdir + '/user query/'
        _q_file = _q_subdir + _q_file
        
        with open(_q_file, "rb") as f:
            inmemory_file = io.BytesIO(f.read())
        bw = load_workbook(inmemory_file, read_only = True)
    
        _query = pd.read_excel(_q_file, sheet_name = "query", index_col = 0)
        
    else:
        print("Type SMILES")
        _query = str(input())  
    
    print("Select similarity metrics:", '\n', "1 - Tanimoto coefficient", '\n', "2 - Euclidean distance", '\n', "3 - Levenshtein coefficient", '\n', "4 - Cosine similarity")
    _method = int(input())
    return(_database, _query, _method)