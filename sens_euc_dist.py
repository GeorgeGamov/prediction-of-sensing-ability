# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Authors: AKsenofontov, MLukanov                            #
# Date: 2023                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
from rdkit import Chem
from rdkit import DataStructs
from rdkit.DataManip.Metric.rdMetricMatrixCalc import GetEuclideanDistMat

def get_fgp(row):
    try:
        fgp = Chem.RDKFingerprint(Chem.MolFromSmiles(row['SMILES'],sanitize=False))
        return fgp
    except:
        return None
        
def euclidean_distance(fgp1,fgp2):
    a = sum(fgp1)
    b = sum(fgp2)
    c = 0
    for index in range(len(fgp1)):
        if fgp1[index] == 1 and fgp2[index] == 1:
            c += 1
    return (a + b - 2 * c) ** (0.5)

def sens_euc_dist(_q_SMILES, df_one_cation):
    
    euclidean_dist = []
    df_one_cation['fingerprinting'] = df_one_cation.apply(get_fgp, axis=1)
    
    fgp1 = Chem.RDKFingerprint(Chem.MolFromSmiles(_q_SMILES[0], sanitize=False))
    
    for fgp2 in df_one_cation['fingerprinting']:
        euclidean_dist.append(euclidean_distance(fgp1,fgp2))
    
    coefficient_df = df_one_cation.copy()
    coefficient_df['euclidean_distance'] = euclidean_dist
    
    # best euclidian distances
    df_best_euclidean = (coefficient_df
                       .sort_values(by='euclidean_distance')
                       .head(10)
                       .loc[:,['SMILES','Cation','euclidean_distance']])
                       
    return df_best_euclidean