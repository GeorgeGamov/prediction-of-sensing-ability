# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import numpy as np

# basic preprocessing ----------------------------------------
def sens_preproc(_database, _query, _q_file):
# checking if database contains duplicate entries and cleaning from duplicates  
    #_database_cleaned = _database.drop_duplicates(subset='InChIKey', keep='first', inplace=False)
    _database_cleaned = _database
# converting database
    _db_SMILES = _database_cleaned['SMILES'].to_numpy()
    _db_InChI = _database_cleaned['InChI'].to_numpy()
    _db_InChIKey = _database_cleaned['InChIKey'].to_numpy()
    _db_cation = _database_cleaned['Cation'].to_numpy()
    _db_medium = _database_cleaned['Medium'].to_numpy()
    _db_source = _database_cleaned['Source'].to_numpy()

# converting query
    if _q_file != "":
    
        _q_SMILES = _query['SMILES'].to_numpy()
        _q_InChI = _query['InChI'].to_numpy()
        _q_InChIKey = _query['InChIKey'].to_numpy()
        
    else:
    
         _q_SMILES = [_query]
         _q_InChI = [""]
         _q_InChIKey = [""]
    
    return(_db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, _q_InChI, _q_InChIKey)
