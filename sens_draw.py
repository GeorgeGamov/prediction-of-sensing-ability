# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Authors: AKsenofontov, MLukanov                            #
# Date: 2023                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
from rdkit import Chem
from rdkit.Chem import Draw

#drawing
def sens_draw_mol(df_out):
    img = Draw.MolsToGridImage(
            [Chem.MolFromSmiles(smi) for smi in df_out['SMILES']],
            molsPerRow=4,
            subImgSize=(250,250),
            legends=[x for x in df_out['Cation']])
            
    display(img)
    display(df_out)          
    