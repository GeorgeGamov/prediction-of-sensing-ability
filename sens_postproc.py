# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import numpy as np
import pandas as pd

# basic postprocessing ---------------------------------------
def sens_postproc(res_ret_cation, res_ret_medium, res_ret_source, res_ret_db_SMILES, sim_N_max, _q_SMILES, _q_InChI,
                  _q_InChIKey, N_max):
    
    entry_out = [['' for i in range(N_max)] for j in range(len(_q_SMILES))]
    SMILES_out = [['' for i in range(N_max)] for j in range(len(_q_SMILES))]
    InChI_out = [['' for i in range(N_max)] for j in range(len(_q_SMILES))]
    InChIKey_out = [['' for i in range(N_max)] for j in range(len(_q_SMILES))]
    
    for i in range(len(_q_SMILES)):
        entry_out[i][0] = str(i+1) 
        SMILES_out[i][0] = _q_SMILES[i]
        InChI_out[i][0] = _q_InChI[i]
        InChIKey_out[i][0] = _q_InChIKey[i]
    
    entry_out = (np.array(entry_out)).reshape(len(_q_SMILES) * N_max, 1)
    SMILES_out = (np.array(SMILES_out)).reshape(len(_q_SMILES) * N_max, 1)
    InChI_out = (np.array(InChI_out)).reshape(len(_q_SMILES) * N_max, 1)
    InChIKey_out = (np.array(InChIKey_out)).reshape(len(_q_SMILES) * N_max, 1)
    cation_out = (np.array(res_ret_cation)).reshape(len(_q_SMILES) * N_max, 1)
    medium_out = (np.array(res_ret_medium)).reshape(len(_q_SMILES) * N_max, 1)
    source_out = (np.array(res_ret_source)).reshape(len(_q_SMILES) * N_max, 1)
    param_out = (np.array(sim_N_max)).reshape(len(_q_SMILES) * N_max, 1)
    db_SMILES_out = (np.array(res_ret_db_SMILES)).reshape(len(_q_SMILES) * N_max, 1)
    
    df_out = (pd.DataFrame(np.hstack((entry_out, SMILES_out, InChI_out, InChIKey_out, cation_out, medium_out, source_out,
                        param_out, db_SMILES_out)), columns = ['Entry', 'SMILES queried', 'InChI queried',
                        'InChIKey queried', 'Cation', 'Medium', 'Reference', 'Database SMILES', 'Similarity']))
    
    
    return(df_out)