# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import numpy as np
from Levenshtein import ratio
import pandas as pd
# measuring Levenshtein ratio  -------------------------------
def sens_levenstein_proc(_db_SMILES, _db_InChI, _db_InChIKey, _db_cation, _db_medium, _db_source, _q_SMILES, 
                         _q_InChI, _q_InChIKey, N_max):
    
    # initialize necessary variables    
    _db_InChIKey = np.array(_db_InChIKey).tolist()
    res_ret_cation = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_medium = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_source = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    res_ret_db_SMILES = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    lr = []
    sim_N_max = [[0 for i in range(N_max)] for j in range(len(_q_InChIKey))]
    
    for i in range(len(_q_InChIKey)):
        for j in range(len(_db_SMILES)):
            lr.append(ratio(_q_SMILES[i], _db_SMILES[j]))
    
        indices_N_max = (sorted(range(len(lr)), key = lambda sub: lr[sub])[-N_max:])[::-1]
        for k in range(N_max):
            sim_N_max[i][k] = lr[indices_N_max[k]]
            res_ret_cation[i][k] = _db_cation[indices_N_max[k]]
            res_ret_medium[i][k] = _db_medium[indices_N_max[k]]
            res_ret_source[i][k] = _db_source[indices_N_max[k]]
            res_ret_db_SMILES[i][k] = _db_SMILES[indices_N_max[k]]
                
        lr.clear() 
        
        df_best_levenshtein = pd.DataFrame(np.hstack(((np.array(res_ret_db_SMILES)).reshape(len(_q_SMILES) * N_max, 1), (np.array(res_ret_cation, dtype = object)).reshape(len(_q_SMILES) * N_max, 1), (np.array(sim_N_max)).reshape(len(_q_SMILES) * N_max, 1))), index = indices_N_max, columns = ['SMILES','Cation','levenshtein_coefficients'])
        
        df_best_levenshtein['Cation'] = df_best_levenshtein['Cation'].apply(lambda x: ','.join(x))
                
    return(res_ret_cation, res_ret_medium, res_ret_source, sim_N_max, res_ret_db_SMILES, df_best_levenshtein)