# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Authors: AKsenofontov, MLukanov                            #
# Date: 2023                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import pandas as pd
# ------------------------------------------------------------
def one_cation(row):
    if len(row['Cation']) < 2:
        return True
    else:
        return False
        
def sens_get_cat_for_fp(_database):

    _database['Cation'] = _database['Cation'].str.split(',')
    df_one_cation = _database.loc[_database.apply(one_cation, axis=1)].reset_index(drop=True)
    df_not_one_cation = _database.loc[~(_database.apply(one_cation, axis=1))].reset_index(drop=True)
    
    smiles_list=[]
    cation_list=[]
    for index in range(len(df_not_one_cation)):
        for cation in df_not_one_cation.loc[index,'Cation']:
            smiles_list.append(df_not_one_cation.loc[index,'SMILES'])
            cation = cation.replace(' ','')
            cation_list.append(cation)

    df2 = pd.DataFrame({'SMILES':smiles_list, 'Cation':cation_list})
    df_one_cation = pd.concat([df_one_cation,df2], ignore_index = True)
    df_one_cation['Cation'] = df_one_cation['Cation'].apply(lambda x: x[0] if type(x)!= str else x)
    df_one_cation = df_one_cation.drop_duplicates()
    df_one_cation = df_one_cation.dropna()
    
    return df_one_cation
    
def sens_cat_distr(df_one_cation):
    
    distribution = (df_one_cation
                .pivot_table(index = 'Cation', values='SMILES', aggfunc='count')
                .sort_values(by='SMILES', ascending=False))
    distribution.columns=['count']
    distribution['id'] = distribution.index.where(distribution['count']>=10, 'others')
    df_one_cation['id']=df_one_cation['Cation'].apply(lambda x: distribution['id'][x])
    display(distribution)
    display(distribution.pivot_table(index='id', values='count', aggfunc='sum').plot(y='count', kind='pie', legend=False))