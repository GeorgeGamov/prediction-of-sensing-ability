# ########################################################## #
#                                                            #
# Name: KEV:Prediction of sensing ability                    #
# Authors: AKsenofontov, MLukanov                            #
# Date: 2023                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------

def sens_united_metrics(df_best_tanimoto, df_best_euclidean, df_best_levenshtein, df_best_doc2vec):
    #display(df_best_tanimoto)
    #display(df_best_euclidean)
    #display(df_best_levenshtein)
    #display(df_best_doc2vec)
    best_tan_euc_lev_doc = ((df_best_tanimoto.join(df_best_euclidean['euclidean_distance'])).join(df_best_levenshtein['levenshtein_coefficients'])).join(df_best_doc2vec['cosine_similarity']).dropna()
    display(best_tan_euc_lev_doc)
    return best_tan_euc_lev_doc