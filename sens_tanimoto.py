# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Authors: AKsenofontov, MLukanov                            #
# Date: 2023                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import Draw

def get_fgp(row):
    try:
        fgp = Chem.RDKFingerprint(Chem.MolFromSmiles(row['SMILES'],sanitize=False))
        return fgp
    except:
        return None

def sens_tanimoto(_q_SMILES, df_one_cation):

    tanimoto_coefficients = []
    df_one_cation['fingerprinting'] = df_one_cation.apply(get_fgp, axis=1)
    
    fgp1 = Chem.RDKFingerprint(Chem.MolFromSmiles(_q_SMILES[0], sanitize=False))
    
    for fgp2 in df_one_cation['fingerprinting']:
        tanimoto_coefficients.append(DataStructs.FingerprintSimilarity(fgp1,fgp2))
        
    coefficient_df = df_one_cation.copy()
    coefficient_df['tanimoto_coefficients']=tanimoto_coefficients
    
    # best tanimoto_coefficients
    df_best_tanimoto=(coefficient_df.
                      sort_values(by='tanimoto_coefficients', ascending = False)
                      .head(10)
                      .loc[:,['SMILES','Cation','tanimoto_coefficients']])
    
    return df_best_tanimoto