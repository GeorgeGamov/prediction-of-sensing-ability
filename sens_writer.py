# ########################################################## #
#                                                            #
# Name: Prediction of sensing ability                        #
# Author: GGamov                                             #
# Date: 2022                                                 #
#                                                            #
# ########################################################## #

# import libraries -------------------------------------------
import pandas as pd
import os
import sys
from pathlib import Path
sys.path.append(os.path.abspath('../kev/'))

# basic download ---------------------------------------------

def sens_output(_subdir, _file_out, df_out):
    
    if _subdir != '':
        _subdir = '/' + _subdir
    _subdir = '../../output' + _subdir + '/'
    
    path = Path(_subdir)
    if not path.exists():    
        path.mkdir(parents = True, exist_ok = True)
        
    # single file (xlsx)
    
    if _file_out != "":
        
        _file_out = _subdir + _file_out
        
        # output
        with pd.ExcelWriter(_file_out, mode = "w") as output: # specify the path!
            df_out.to_excel(output, sheet_name = 'Prediction results', index = False)